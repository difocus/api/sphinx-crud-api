# Description of Sphinxsearch CRUD API

## Create client

```php

use ShopExpress\SphinxSearchClient\Entity\ShopsEntityFactory;
use ShopExpress\SphinxSearchClient\SphinxClient;
use ShopExpress\SphinxSearchClient\SphinxClientConfiguration;

$clientConfiguration = (new SphinxClientConfiguration())
        ->setBaseUrl('http://search.ru/')
        ->setApiToken('123')
        ->setSiteId(121)
        ->setIndexName('shop_id');
$shopsClient = new SphinxClient($clientConfiguration, new ShopsEntityFactory());

```

## Create Entity

```php
use ShopExpress\SphinxSearchClient\Entity\ShopsEntity;
use ShopExpress\SphinxSearchClient\Entity\ShopsEntityFactory;

// №1
$entity_1 = (new ShopsEntityFactory())
  ->create([
     'id' => 1,
     'oid' => 1,
     'name' => 'test name',
     'value' => 'test value',
  ]);            
$shopsClient->createEntity($entity_1);

// №2
/* @var $entity_2 ShopsEntity */
$entity_2 = $shopsClient->createEntity((new ShopsEntity)
        ->setProp('id', 2)
        ->setProp('oid', 1)
        ->setProp('name', 'test name')
        ->setProp('value', 'test value'));

```

## Update Entity

```php
use ShopExpress\SphinxSearchClient\Entity\ShopsEntity;
use ShopExpress\SphinxSearchClient\Entity\ShopsEntityFactory;

// №1
$entity_1 = (new ShopsEntityFactory())
  ->modify($entity_1, ['oid' => 3]);
    
$shopsClient->updateEntity($entity_1);

// №2
/* @var $entity_2 ShopsEntity */
$entity_2 = $shopsClient->updateEntity($entity_2
        ->setProp('oid', 3));

```

## Delete Entity

```php
use ShopExpress\SphinxSearchClient\Entity\ShopsEntity;

// №1
$shopsClient->removeEntity($entity_1);

// №2
$shopsClient->removeEntity((new ShopsEntity)
        ->setProp('id', 1));

```

## Delete Entity by attributes

```php

$shopsClient->removeByAttributes(['oid' => 3]);

```

## Match Entity

```php
use ShopExpress\SphinxSearchClient\Entity\ShopsEntity;

/* @var $entities ShopsEntity[] */
$entities = $shopsClient->matchEntity('Ёжики', [
	'attributes' => [
		'subtitle'
	],
	'values' => [
		'Пыжики'
	],
	'operators' => [
		'=' // '=', '<', '>', '<=', '>=', '!='
	]
]);

```

## Match Entity By Query

```php
use ShopExpress\SphinxSearchClient\Entity\ShopsEntity;
use ShopExpress\SphinxSearchClient\SphinxQueryBuilder;

$query = new SphinxQueryBuilder();
$query = $query->select('*')
    ->from($shopsClient->getConfiguration()->getIndexName())
    ->match('*', 'светодиодное')
    ->where('oid', '=', 5526);

/* @var $entities ShopsEntity[] */
$entities = $shopsClient->execute($query);

```

## Find Entity

```php
use ShopExpress\SphinxSearchClient\Entity\ShopsEntity;

/* @var $entities ShopsEntity[] */
$entities = $shopsClient->findEntity(['oid' => 3]);

```

## Get Entity

```php
use ShopExpress\SphinxSearchClient\Entity\ShopsEntity;

/* @var $entities ShopsEntity[] */
$entities = $shopsClient->getEntity(1);


```
