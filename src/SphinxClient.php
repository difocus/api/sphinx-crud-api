<?php
namespace ShopExpress\SphinxSearchClient;

use InvalidArgumentException;
use ShopExpress\SphinxSearchClient\Entity\EntityFactoryInterface;
use ShopExpress\SphinxSearchClient\Entity\EntityInterface;
use ShopExpress\SphinxSearchClient\Exception\ApiKeyException;
use ShopExpress\SphinxSearchClient\Exception\ConnectException;
use ShopExpress\SphinxSearchClient\Exception\EmptyAnswerException;
use ShopExpress\SphinxSearchClient\Exception\EmptyQueryException;
use ShopExpress\SphinxSearchClient\Exception\QueryException;

/**
 * Class SphinxClient
 * @package ShopExpress\SphinxSearchClient
 */
class SphinxClient
{
    public const HTTP_OK = 200;
    public const HTTP_AUTH = 401;

    public const COMPRESS_LIMIT = 2000;

    /**
     * @var SphinxClientConfiguration
     */
    private $configuration;
    /**
     * @var EntityFactoryInterface
     */
    private $entityFactory;

    /**
     * @param SphinxClientConfiguration $configuration
     * @param EntityFactoryInterface $entityFactory
     *
     */
    public function __construct(SphinxClientConfiguration $configuration, EntityFactoryInterface $entityFactory)
    {
        $this->configuration = $configuration;
        $this->entityFactory = $entityFactory;
    }

    /**
     * Create an entity
     *
     * @param EntityInterface $entity The data
     *
     * @throws ApiKeyException
     * @throws ConnectException
     * @throws EmptyAnswerException
     * @throws QueryException
     * @return EntityInterface The Record entity.
     */
    public function createEntity(EntityInterface $entity): EntityInterface
    {
        $this->makeRequest("{$this->configuration->getBaseUrl()}/{$this->configuration->getIndexName()}", 'POST', $this->serializeEntity($entity));

        //$entity->id = $data['success']['id']; // id return false
        return $entity;
    }

    /**
     * Select entity
     *
     * @param string|null $search Request
     * @param array $attributes The attributes
     *
     * @param array $options
     * @param int $limit
     * @param int $offset
     * @param array $fields
     *
     * @throws ApiKeyException
     * @throws ConnectException
     * @throws EmptyAnswerException
     * @throws EmptyQueryException
     * @throws QueryException
     * @return EntityInterface[]
     */
    public function matchEntity(?string $search = null, array $attributes = [], array $options = [], int $limit = 20, int $offset = 0, array $fields = ['*']): array
    {
        $qb = (new SphinxQueryBuilder())
            ->select($fields)
            ->from($this->configuration->getIndexName());

        if (!empty($search)) {
            $qb->match('*', $search);
        }

        foreach ($attributes as $attribute => $value) {
            $qb->where($attribute, $value);
        }

        foreach ($options as $option => $value) {
            $qb->option($option, $value);
        }

        $qb->where($this->configuration->getSiteIdAlias(), $this->configuration->getSiteId())
            ->limit($offset, $limit);

        $data = $this->execute($qb);

        $return = [];
        foreach ($data as $item) {
            $return[] = $this->entityFactory->factory($item);
        }

        return $return;
    }

    /**
     * @param array $attributes
     * @param array $options
     * @param int $limit
     * @param int $offset
     *
     * @throws ApiKeyException
     * @throws ConnectException
     * @throws EmptyAnswerException
     * @throws EmptyQueryException
     * @throws QueryException
     * @return array
     */
    public function findEntityByAttributes(array $attributes, array $options = [], int $limit = 20, int $offset = 0): array
    {
        return $this->matchEntity(null, $attributes, $options, $limit, $offset);
    }

    /**
     * Get entity
     *
     * @param int $id
     *
     * @throws ApiKeyException
     * @throws ConnectException
     * @throws EmptyAnswerException
     * @throws QueryException
     * @return EntityInterface[]
     */
    public function getEntity(int $id): array
    {
        $data = $this->makeRequest("{$this->configuration->getBaseUrl()}/{$this->configuration->getIndexName()}/{$this->getInternalId($id)}", 'GET');

        $return = [];
        foreach ($data as $item) {
            $return[] = $this->entityFactory->factory($item);
        }
        return $return;
    }

    /**
     * Create or replace
     *
     * @param EntityInterface $entity
     *
     * @throws ApiKeyException
     * @throws ConnectException
     * @throws EmptyAnswerException
     * @throws QueryException
     * @return EntityInterface
     */
    public function updateEntity(EntityInterface $entity): EntityInterface
    {
        $this->makeRequest("{$this->configuration->getBaseUrl()}/{$this->configuration->getIndexName()}", 'PUT', $this->serializeEntity($entity));

        return $entity;
    }

    /**
     * Removes a entity
     *
     * @param EntityInterface $entity
     *
     * @throws ApiKeyException
     * @throws ConnectException
     * @throws EmptyAnswerException
     * @throws QueryException
     * @return EntityInterface The removed Record entity.
     */
    public function removeEntity(EntityInterface $entity): EntityInterface
    {
        if (!$entityId = $entity->getId()) {
            throw new InvalidArgumentException('Parameter `id` is required.');
        }

        $this->makeRequest("{$this->configuration->getBaseUrl()}/{$this->configuration->getIndexName()}/{$this->getInternalId($entityId)}", 'DELETE');

        return $entity;
    }

    /**
     * Deletes entity by attributes
     *
     * @param array $attributes
     *
     * @throws ApiKeyException
     * @throws ConnectException
     * @throws EmptyAnswerException
     * @throws EmptyQueryException
     * @throws QueryException
     */
    public function removeByAttributes(array $attributes): void
    {
        if (empty($attributes)) {
            throw new InvalidArgumentException('Empty attributes', 500);
        }

        $qb = (new SphinxQueryBuilder())
            ->delete()
            ->from($this->configuration->getIndexName());

        foreach ($attributes as $attribute => $value) {
            $qb->where($attribute, $value);
        }

        $qb->where($this->configuration->getSiteIdAlias(), $this->configuration->getSiteId());

        try {
            $this->execute($qb);
        } catch (QueryException $e) {
            // Sphinx bug...
            if ($e->getCodeString() !== 'HY000') {
                throw $e;
            }
        }
    }

    /**
     * @param SphinxQueryBuilder $queryBuilder
     *
     * @throws ApiKeyException
     * @throws ConnectException
     * @throws EmptyAnswerException
     * @throws EmptyQueryException
     * @throws QueryException
     * @return array
     */
    public function execute(SphinxQueryBuilder $queryBuilder): array
    {
        if (!$rawQuery = $queryBuilder->compile()->getCompiled()) {
            throw new EmptyQueryException('Query is empty.');
        }

        return $this->makeRequest(
            "{$this->configuration->getBaseUrl()}/raw",
            'GET',
            [
                'raw' => $rawQuery,
                'type' => $queryBuilder->getType(),
                'params' => $queryBuilder->getBindings(),
            ]
        );
    }

    /**
     * @return SphinxClientConfiguration
     */
    public function getConfiguration(): SphinxClientConfiguration
    {
        return $this->configuration;
    }

    /**
     * Serializes $entity to array, adds index unique key and site id
     *
     * @param EntityInterface $entity
     *
     * @return array
     */
    private function serializeEntity(EntityInterface $entity): array
    {
        return array_merge($entity->toArray(), [
            'id' => $this->getInternalId($entity->getId()),
            $this->configuration->getSiteIdAlias() => $this->configuration->getSiteId(),
        ]);
    }

    /**
     * Returns internal unique key for index row
     *
     * @param int $entityId
     *
     * @return int
     */
    private function getInternalId(int $entityId): int
    {
        return crc32(sprintf('%d_%s', $entityId, $this->configuration->getSiteId()));
    }

    /**
     * Makes a request
     *
     * @param string $objectUrl
     * @param string $method
     * @param array $data
     *
     * @throws ApiKeyException
     * @throws ConnectException
     * @throws EmptyAnswerException
     * @throws QueryException
     *
     * @return array
     */
    private function makeRequest(string $objectUrl, string $method, array $data = []): array
    {
        $ch = curl_init();
        $opts = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => 'dicms-api-php-1.0',
            CURLOPT_CONNECTTIMEOUT => $this->configuration->getConnectTimeout(),
            CURLOPT_TIMEOUT => $this->configuration->getTimeout(),
            CURLOPT_URL => $objectUrl . '?' . http_build_query([
                    'token' => $this->configuration->getApiToken(),
                ]),
        ];

        switch ($method) {
            case 'GET':
            case 'PUT':
            case 'POST':
            case 'PATCH':
            case 'DELETE':
                $postData = json_encode($data);

                if (strlen($postData) > self::COMPRESS_LIMIT) {
                    $postData = gzcompress($postData, 6);
                }

                $opts[CURLOPT_CUSTOMREQUEST] = $method;
                $opts[CURLOPT_RETURNTRANSFER] = true;
                $opts[CURLOPT_POSTFIELDS] = $postData;
                $opts[CURLOPT_HTTPHEADER][] = 'Content-Length: ' . strlen($postData);
                break;
        }

        curl_setopt_array($ch, $opts);
        $resultBody = curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($statusCode === self::HTTP_AUTH) {
            throw new ApiKeyException($this->configuration->getApiToken());
        }

        if ($statusCode !== self::HTTP_OK) {
            throw new ConnectException($objectUrl);
        }

        if ($resultBody === false) {
            throw new EmptyAnswerException($objectUrl);
        }

        if (null === ($result = json_decode($resultBody, true))) {
            return [];
        }

        if (isset($result['error'])) {
            if ($result['error']['message'] === 'Invalid API token!') {
                throw new ApiKeyException($this->configuration->getApiToken());
            }
            if ($result['error']['message'] === 'No Content') {
                return [];
            }
            throw new QueryException($objectUrl, $result['error']['code'], $result['error']['message'], $data);
        }

        return $result;
    }
}