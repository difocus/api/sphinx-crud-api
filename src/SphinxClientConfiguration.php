<?php


namespace ShopExpress\SphinxSearchClient;


/**
 * Class SphinxClientConfiguration
 * @package ShopExpress\SphinxSearchClient
 */
class SphinxClientConfiguration
{
    public const CURLOPT_TIMEOUT = 3;
    public const CURLOPT_CONNECTTIMEOUT = 1;

    /**
     * @var string
     */
    private $baseUrl;
    /**
     * @var string
     */
    private $apiToken;
    /**
     * @var string
     */
    private $indexName;
    /**
     * @var int
     */
    private $siteId;
    /**
     * @var string
     */
    private $siteIdAlias = 'site_id';
    /**
     * @var int
     */
    private $timeout = self::CURLOPT_TIMEOUT;
    /**
     * @var int
     */
    private $connectTimeout = self::CURLOPT_CONNECTTIMEOUT;

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     *
     * @return SphinxClientConfiguration
     */
    public function setBaseUrl(string $baseUrl): SphinxClientConfiguration
    {
        $this->baseUrl = trim($baseUrl, '/');
        return $this;
    }

    /**
     * @return string
     */
    public function getApiToken(): string
    {
        return $this->apiToken;
    }

    /**
     * @param string $apiToken
     *
     * @return SphinxClientConfiguration
     */
    public function setApiToken(string $apiToken): SphinxClientConfiguration
    {
        $this->apiToken = $apiToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getIndexName(): string
    {
        return $this->indexName;
    }

    /**
     * @param string $indexName
     *
     * @return SphinxClientConfiguration
     */
    public function setIndexName(string $indexName): SphinxClientConfiguration
    {
        $this->indexName = $indexName;
        return $this;
    }

    /**
     * @return int
     */
    public function getSiteId(): int
    {
        return $this->siteId;
    }

    /**
     * @param int $siteId
     *
     * @return SphinxClientConfiguration
     */
    public function setSiteId(int $siteId): SphinxClientConfiguration
    {
        $this->siteId = $siteId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSiteIdAlias(): string
    {
        return $this->siteIdAlias;
    }

    /**
     * @param string $siteIdAlias
     *
     * @return SphinxClientConfiguration
     */
    public function setSiteIdAlias(string $siteIdAlias): SphinxClientConfiguration
    {
        $this->siteIdAlias = $siteIdAlias;
        return $this;
    }

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     *
     * @return SphinxClientConfiguration
     */
    public function setTimeout(int $timeout): SphinxClientConfiguration
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @return int
     */
    public function getConnectTimeout(): int
    {
        return $this->connectTimeout;
    }

    /**
     * @param int $connectTimeout
     *
     * @return SphinxClientConfiguration
     */
    public function setConnectTimeout(int $connectTimeout): SphinxClientConfiguration
    {
        $this->connectTimeout = $connectTimeout;
        return $this;
    }
}