<?php

namespace ShopExpress\SphinxSearchClient\Console;

use Composer\Installer\PackageEvent;
use Composer\Script\Event;
use InvalidArgumentException;

/**
 * Class Install
 * @package ShopExpress\SphinxSearchClient\Console
 */
class Install
{
    /**
     * @param Event $event
     */
    public static function postInstall(Event $event): void
    {
        $config = '';
        $io = $event->getIO();
        $arg = $io->ask('Enter base url (e.g. http://site.ru): ');
        $config .= 'BASE_URL=' . escapeshellcmd($arg);
        $config .= "\n";

        $arg = $io->ask('Enter api token (or leave empty for generate): ');
        $config .= 'API_TOKEN=' . (escapeshellcmd($arg) ?: md5(microtime(true)));
        $config .= "\n";

        $arg = $io->ask('Enter site ID: ');
        $config .= 'SITE_ID=' . escapeshellcmd($arg);
        $config .= "\n";

        file_put_contents('.env', $config);

        $arg = $io->askConfirmation('Copy "example/index.php" file to root directory (yes/no, default - yes)? ', true);
        if (escapeshellcmd($arg)) {
            try {
                copy('example/index.php', 'index.php');
            } catch (IOException $e) {
                throw new InvalidArgumentException('<error>Could not copy "example/index.php"</error>');
            }
        } else {
            $io->write('File "example/index.php" wasn\'t copied');
        }

        $io->write('Sphinxsearch Crud Api Successfully installed!');
    }
}