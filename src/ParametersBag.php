<?php


namespace ShopExpress\SphinxSearchClient;


/**
 * Class ParametersBag
 * @package ShopExpress\SphinxSearchClient
 */
class ParametersBag
{
    /**
     * @var array
     */
    private $parameters = [];
    /**
     * @var int
     */
    private $placeholderIncrement = 0;

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function addParameters(array $parameters): array
    {
        return array_map(function ($parameter) {
            return $this->addParameter($parameter);
        }, $parameters);
    }

    /**
     * @param $parameter
     *
     * @return string
     */
    public function addParameter($parameter): string
    {
        $parameterName = ':p' . ($this->placeholderIncrement++);

        $this->parameters[$parameterName] = $parameter;

        return $parameterName;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function flush(): void
    {
        $this->parameters = [];
        $this->placeholderIncrement = 0;
    }
}
