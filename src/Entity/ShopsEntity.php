<?php

namespace ShopExpress\SphinxSearchClient\Entity;

/**
 * Class ShopsEntity
 * @package ShopExpress\SphinxSearchClient\Entity
 */
class ShopsEntity implements EntityInterface
{
    public static $indexName = 'shops';
    public static $siteIdAlias = 'shop_id';

    public $oid;
    public $name;
    public $content;

    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->oid;
    }

    /**
     * Sets the property.
     *
     * @param string $name The name
     * @param mixed $value The value
     *
     * @return EntityInterface|self
     */
    public function setProp(string $name, $value): EntityInterface
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        }
        return $this;
    }

    /**
     * Returns a array representation of the object.
     *
     * @return array Array representation of the object.
     */
    public function toArray(): array
    {
        return [
            'oid' => $this->oid,
            'name' => $this->name,
            'content' => $this->content,
        ];
    }
}
