<?php
namespace ShopExpress\SphinxSearchClient\Entity;

/**
 * Interface EntityInterface
 * @package ShopExpress\SphinxSearchClient\Entity
 */
interface EntityInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param string $name
     * @param string $value
     *
     * @return EntityInterface
     */
    public function setProp(string $name, string $value): EntityInterface;

    /**
     * @return array
     */
    public function toArray(): array;
}
