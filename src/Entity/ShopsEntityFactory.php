<?php


namespace ShopExpress\SphinxSearchClient\Entity;


/**
 * Class ShopsEntityFactory
 * @package ShopExpress\SphinxSearchClient\Entity
 */
class ShopsEntityFactory implements EntityFactoryInterface
{
    /**
     * @param $array
     *
     * @return EntityInterface
     */
    public function factory(array $array): EntityInterface
    {
        return $this->create($array);
    }

    /**
     * @param array $array
     *
     * @return EntityInterface
     */
    public function create(array $array): EntityInterface
    {
        $entity = new ShopsEntity();

        return $this->modify($entity, $array);
    }

    public function modify(EntityInterface $entity, array $array): EntityInterface
    {
        foreach ($array as $attr => $value) {
            $entity->setProp($attr, $value);
        }

        return $entity;
    }
}
