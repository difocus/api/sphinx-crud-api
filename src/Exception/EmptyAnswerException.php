<?php

namespace ShopExpress\SphinxSearchClient\Exception;


use Exception;

/**
 * Class EmptyAnswerException
 * @package ShopExpress\SphinxSearchClient\Exception
 */
class EmptyAnswerException extends Exception
{
    private $tplMessage = 'Пустой ответ при подключении к %s';

    /**
     * EmptyAnswerException constructor.
     *
     * @param string $string
     */
    public function __construct(string $string)
    {
        parent::__construct(sprintf($this->tplMessage, $string), 500);
    }
}