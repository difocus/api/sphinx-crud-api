<?php

namespace ShopExpress\SphinxSearchClient\Exception;


use Exception;

/**
 * Class ApiKeyException
 * @package ShopExpress\SphinxSearchClient\Exception
 */
class ApiKeyException extends Exception
{
    private $tplMessage = 'Невозможно присоединиться к Sphinx серверу с ключем `%s`';

    /**
     * ApiKeyException constructor.
     *
     * @param string $string
     */
    public function __construct(string $string)
    {
        parent::__construct(sprintf($this->tplMessage, $string), 500);
    }
}