<?php

namespace ShopExpress\SphinxSearchClient\Exception;


use Exception;

/**
 * Class EmptyQueryException
 * @package ShopExpress\SphinxSearchClient\Exception
 */
class EmptyQueryException extends Exception
{
}