<?php

namespace ShopExpress\SphinxSearchClient\Exception;


use Exception;

/**
 * Class QueryException
 * @package ShopExpress\SphinxSearchClient\Exception
 */
class QueryException extends Exception
{
    /**
     * @var string
     */
    private $tplMessage = 'Ошибка при запросе к %s: %s';
    /**
     * @var array
     */
    private $context;
    /**
     * @var string
     */
    private $codeString;

    /**
     * EmptyAnswerException constructor.
     *
     * @param string $url
     * @param string $codeString
     * @param string $message
     * @param array $context
     */
    public function __construct(string $url, string $codeString, string $message, array $context = [])
    {
        parent::__construct(sprintf($this->tplMessage, $url, $message), 500);

        $this->codeString = $codeString;
        $this->context = $context;
    }

    /**
     * @return string
     */
    public function getCodeString(): string
    {
        return $this->codeString;
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }
}