<?php


namespace ShopExpress\SphinxSearchClient\Exception;


use Exception;

/**
 * Class ConnectException
 * @package ShopExpress\SphinxSearchClient\Exception
 */
class ConnectException extends Exception
{
    private $tplMessage = 'Невозможно присоединиться к sphinx серверу %s';

    /**
     * ConnectException constructor.
     *
     * @param string $string
     */
    public function __construct(string $string)
    {
        parent::__construct(sprintf($this->tplMessage, $string), 500);
    }
}