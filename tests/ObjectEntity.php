<?php


namespace ShopExpress\SphinxSearchClient\Tests;


use ShopExpress\SphinxSearchClient\Entity\EntityInterface;

class ObjectEntity implements EntityInterface
{
    public static $indexName = 'shops';
    public static $siteIdAlias = 'shop_id';

    public $oid;
    public $type;
    public $name;
    public $title;
    public $annotate;
    public $text;
    public $articul;
    public $weight;
    public $meta_keywords;
    public $meta_description;
    public $price;
    public $vendor;

    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->oid;
    }

    /**
     * @param string $name
     * @param string $value
     *
     * @return EntityInterface
     */
    public function setProp(string $name, $value): EntityInterface
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        }
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'oid' => $this->oid,
            'type' => $this->type,
            'name' => $this->name,
            'title' => $this->title,
            'annotate' => $this->annotate,
            'text' => $this->text,
            'articul' => $this->articul,
            'weight' => $this->weight,
            'meta_keywords' => $this->meta_keywords,
            'meta_description' => $this->meta_description,
            'price' => $this->price,
            'vendor' => $this->vendor,
        ];
    }
}
