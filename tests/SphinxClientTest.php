<?php

namespace ShopExpress\SphinxSearchClient\Tests;

use Exception;
use PHPUnit\Framework\TestCase;
use ShopExpress\SphinxSearchClient\Entity\EntityInterface;
use ShopExpress\SphinxSearchClient\Exception\ApiKeyException;
use ShopExpress\SphinxSearchClient\Exception\ConnectException;
use ShopExpress\SphinxSearchClient\Exception\EmptyAnswerException;
use ShopExpress\SphinxSearchClient\Exception\EmptyQueryException;
use ShopExpress\SphinxSearchClient\Exception\QueryException;
use ShopExpress\SphinxSearchClient\SphinxClient;
use ShopExpress\SphinxSearchClient\SphinxClientConfiguration;


/**
 * Сначала запустить SphinxClientTest::testDeleteByAttributes
 *
 * Class SphinxClientTest
 * @package ShopExpress\SphinxSearchClient\Tests
 */
class SphinxClientTest extends TestCase
{
    /**
     * @var int
     */
    private static $entityOid_1 = 501;

    /**
     * @var int
     */
    private static $entityOid_2 = 601;

    /**
     * @var SphinxClient
     */
    private static $client;

    /**
     * @return void
     */
    public static function setUpBeforeClass(): void
    {
        $config = parse_ini_file(__DIR__ . '/../.env');

        $clientConfiguration = (new SphinxClientConfiguration())
            ->setBaseUrl($config['BASE_URL'])
            ->setApiToken($config['API_TOKEN'])
            ->setSiteId($config['SITE_ID'])
            ->setIndexName(ObjectEntity::$indexName)
            ->setSiteIdAlias(ObjectEntity::$siteIdAlias);

        self::$client = new SphinxClient($clientConfiguration, new ObjectEntityFactory());
    }

    /**
     * @throws Exception
     * @return EntityInterface[]
     */
    public function testCreateEntity(): array
    {
        $entityFirst = new ObjectEntity;
        $entityFirst->oid = self::$entityOid_1;
        $entityFirst->type = 7;
        $entityFirst->name = $entityFirst->title = 'Информация по делу о пропаже ёжика';
        $entityFirst->text = 'Задача организации, в особенности же реализация намеченного плана';
        $entityFirst->meta_keywords = 'adasd';
        $entityFirst->meta_description = 'asdasd';
        $entityFirst->vendor = 'asdjahskjd';
        $entityFirst->price = '123';
        $entityFirst->weight = 123;
        $entityFirst->annotate = 'asdadasd';
        $this->assertSame($entityFirst, self::$client->createEntity($entityFirst));

        $entitySecond = new ObjectEntity;
        $entitySecond->oid = self::$entityOid_2;
        $entitySecond->type = 7;
        $entitySecond->name = $entitySecond->title = 'Информация по делу о пропаже кролика';
        $entitySecond->text = 'Задача организации, в особенности же реализация намеченного плана';
        $this->assertSame($entitySecond, self::$client->createEntity($entitySecond));

        return [$entityFirst, $entitySecond];
    }

    /**
     * @depends testCreateEntity
     *
     * @param EntityInterface[] $entities
     *
     * @throws Exception
     * @return mixed
     */
    public function testGetEntity($entities)
    {
        $items = self::$client->getEntity($entities[0]->getId());
        $this->assertCount(1, $items);
        foreach ($items as $entity) {
            $this->assertInstanceOf(ObjectEntity::class, $entity);
        }
        return $entities;
    }

    /**
     * @depends testGetEntity
     *
     * @param EntityInterface[] $entities
     *
     * @throws Exception
     * @return mixed
     */
    public function testMatchEntity($entities)
    {
        $items = self::$client->matchEntity('задача организации');
        $this->assertCount(2, $items);
        foreach ($items as $entity) {
            $this->assertInstanceOf(ObjectEntity::class, $entity);
        }

        $items = self::$client->matchEntity('ёжика');
        $this->assertCount(1, $items);

        $items = self::$client->matchEntity('кролика', ['type' => ['=', 7]]);
        $this->assertCount(1, $items);

        return $entities;
    }

    /**
     * @depends testMatchEntity
     *
     * @param EntityInterface[] $entities
     *
     * @throws Exception
     * @return mixed
     */
    public function testFindEntity($entities)
    {
        $items = self::$client->findEntityByAttributes(['oid' => self::$entityOid_1]);
        $this->assertCount(1, $items);
        $items = self::$client->findEntityByAttributes(['oid' => self::$entityOid_1, 'type' => 7]);
        $this->assertCount(1, $items);

        return $entities;
    }

    /**
     * @depends testFindEntity
     *
     * @param EntityInterface[] $entities
     *
     * @throws ApiKeyException
     * @throws ConnectException
     * @throws EmptyAnswerException
     * @throws EmptyQueryException
     * @return mixed
     */
    public function testLimitEntity($entities)
    {
        $items = self::$client->matchEntity('задача организации', [], [], 1, 1);
        $this->assertCount(1, $items);
        foreach ($items as $entity) {
            $this->assertEquals($entity->articul, $entities[0]->articul);
        }
        return $entities;
    }

    /**
     * @depends testLimitEntity
     *
     * @param EntityInterface[] $entities
     *
     * @throws Exception
     * @return mixed
     */
    public function testUpdateEntity($entities)
    {
        $items = self::$client->getEntity($entities[0]->getId());
        foreach ($items as $entity) {
            $entity->type = 3;
            $this->assertInstanceOf(ObjectEntity::class, self::$client->updateEntity($entity));
        }
        $items = self::$client->getEntity($entities[0]->getId());
        foreach ($items as $entity) {
            $this->assertEquals($entity->type, 3);
        }

        return $entities;
    }

    /**
     * @depends testCreateEntity
     *
     * @param EntityInterface[] $entities
     *
     * @throws Exception
     * @return mixed
     */
    public function testDeleteEntity($entities)
    {
        $items = self::$client->getEntity($entities[0]->getId());
        foreach ($items as $entity) {
            $this->assertInstanceOf(ObjectEntity::class, self::$client->removeEntity($entity));
        }
        $items = self::$client->getEntity($entities[0]->getId());
        $this->assertEmpty($items);
        return $entities;
    }

    /**
     * @throws Exception
     */
    public function testDeleteByAttributes(): void
    {
        try {
            self::$client->removeByAttributes(['oid' => self::$entityOid_1]);
        } catch (QueryException $e) {
        }

        try {
            self::$client->removeByAttributes(['oid' => self::$entityOid_2]);
        } catch (QueryException $e) {
        }

        $this->assertEquals(self::$client->findEntityByAttributes(['oid' => self::$entityOid_1]), []);
        $this->assertEquals(self::$client->findEntityByAttributes(['oid' => self::$entityOid_2]), []);
    }
}
