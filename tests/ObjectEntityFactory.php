<?php


namespace ShopExpress\SphinxSearchClient\Tests;


use InvalidArgumentException;
use ShopExpress\SphinxSearchClient\Entity\EntityFactoryInterface;
use ShopExpress\SphinxSearchClient\Entity\EntityInterface;
use function gettype;

class ObjectEntityFactory implements EntityFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function factory(array $array): EntityInterface
    {
        return $this->create($array);
    }

    /**
     * @inheritDoc
     */
    public function create(array $array): EntityInterface
    {
        $entity = new ObjectEntity();

        return $this->modify($entity, $array);
    }

    /**
     * @inheritDoc
     */
    public function modify(EntityInterface $entity, array $array): EntityInterface
    {
        /** @var ObjectEntity $entity */
        if (!$entity instanceof ObjectEntity) {
            throw new InvalidArgumentException(sprintf('$entity must be an instance of %s, but got %s.', ObjectEntity::class, gettype($entity)));
        }

        foreach ($array as $attr => $value) {
            $entity->setProp($attr, $value);
        }

        return $entity;
    }
}