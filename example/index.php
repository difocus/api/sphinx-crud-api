<?php

if (!file_exists(__DIR__ . '/.env')) {
    exit('.env file does not exists. Please, run composer install command.');
}

$config = parse_ini_file(__DIR__ . '/.env');

require 'vendor/autoload.php';

use ShopExpress\SphinxSearchClient\Entity\ShopsEntity;
use ShopExpress\SphinxSearchClient\Entity\ShopsEntityFactory;
use ShopExpress\SphinxSearchClient\SphinxClient;
use ShopExpress\SphinxSearchClient\SphinxClientConfiguration;

try {
    $clientConfiguration = (new SphinxClientConfiguration())
        ->setBaseUrl($config['BASE_URL'])
        ->setApiToken($config['API_TOKEN'])
        ->setSiteId($config['SITE_ID'])
        ->setIndexName(ShopsEntity::$indexName)
        ->setSiteIdAlias(ShopsEntity::$siteIdAlias);
    $client = new SphinxClient($clientConfiguration, new ShopsEntityFactory());

    // Создание записи 
    /* @var $entity ShopsEntity */
    $entity = $client->createEntity(
        (new ShopsEntity)
            ->setProp('oid', 1)
            ->setProp('name', 'test name')
            ->setProp('value', 'test value')
    );

    // Обновление записи
    $entityFactory = new ShopsEntityFactory();
    $entity = $entityFactory->modify($entity, ['name' => 'test name2']);
    $client->updateEntity($entity);

    // Удаление записи
    $client->removeEntity($entity);
} catch (Throwable $e) {
    echo $e->getMessage();
    echo $e->getTraceAsString();
}